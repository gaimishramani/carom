using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyRewardView : MonoBehaviour
{

    public List<DaysRewardItem> daysRewardItemList;


    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Init()
    {
           for(int i=1; i < daysRewardItemList.Count; i++ )
          {
               daysRewardItemList[i].SetButtonIntractable();
          }
    }

    public void OnClaimButtonClick()
    {
        int index = DailyRewardManager.Instance.dailyRewardData.claimDay + 1;
        DailyRewardManager.Instance.ClaimReward(daysRewardItemList[index].coinReward, daysRewardItemList[index].cashReward);
    }

     public void OnDoubleReward()
    {
        
    }

}
