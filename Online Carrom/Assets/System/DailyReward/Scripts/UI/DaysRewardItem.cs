using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DaysRewardItem : MonoBehaviour
{
    public Button button;
    public int coinReward;
    public int cashReward;
    public int index;
    public Sprite sprite;
    public Image myImage;
    // Start is called before the first frame update

    public void SetButtonIntractable()
    {
        if(DailyRewardManager.Instance.GetClaimedDay() +1 == index)
        {
            myImage.sprite = sprite;
        }
        button.interactable = (DailyRewardManager.Instance.GetClaimedDay() +1 == index);
    }

    public void OnClaimButtonClick()
    {
        DailyRewardManager.Instance.ClaimReward(coinReward, cashReward);
    }

}
