using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

public class DailyRewardManager : MonoBehaviour
{

    public static DailyRewardManager Instance;
    public DailyRewardView dailyRewardView;

    public DailyRewardData dailyRewardData = new DailyRewardData();

    public string DailyRewardDataString
   {
        get{ return PlayerPrefs.GetString("LastUpdatedTime", JsonConvert.SerializeObject(dailyRewardData)); }
        set{  PlayerPrefs.SetString("LastUpdatedTime", value);}
    } 
    
    public void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        LoadData();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartSystem();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartSystem()
    {
       if(dailyRewardData.claimDay ==0)
       {
            dailyRewardView.gameObject.SetActive(true);
       }
       else
       {
             CheckForReward();
       }
    }

    public bool IsFirstDay()
    {
       return dailyRewardData.claimDay == 0;
    }
    
    public int GetClaimedDay()
    {
        return dailyRewardData.claimDay;
    }

    public void CheckForReward()
    {
        TimeSpan diffrance = dailyRewardData.lastUpdateTime - DateTime.Now;
        if(diffrance.TotalHours < 24)
        {
            dailyRewardView.gameObject.SetActive(false);
        }
        else if(diffrance.TotalHours > 24  && diffrance.TotalHours < 48)
        {
            dailyRewardView.gameObject.SetActive(true);
        }
        else
        {
            ResetSystem();
            dailyRewardView.gameObject.SetActive(true);
        }
    
    }

    public void ResetSystem()
    {
        dailyRewardData.claimDay = 0;
    }

    public void ClaimReward(int coin=0, int cash =0)
    {
        dailyRewardData.lastUpdateTime = DateTime.Now;
        dailyRewardData.claimDay  += 1;
        Debug.Log(coin + "Coin");
        Debug.Log(cash + "Cash");
        dailyRewardView.gameObject.SetActive(false);
        SaveData();

    }
         
    public void LoadData()
    {
        dailyRewardData = JsonConvert.DeserializeObject<DailyRewardData>(DailyRewardDataString);
    }

    public void SaveData()
    {
        DailyRewardDataString = JsonConvert.SerializeObject(dailyRewardData);
    }

    public bool IsToday()
    {
        return true;
    }
}

[System.Serializable]
public class DailyRewardData
{
    public DateTime lastUpdateTime = new DateTime();
    public int claimDay = 0;
}
