using UnityEngine;
using UnityEngine.UI;

namespace BBModel
{
	public class BBCanvas : MonoBehaviour
	{
		private Animator animator;

		private CanvasScaler scaller;

		[SerializeField]
		private string trigger;

		private void Awake()
		{
			animator = GetComponent<Animator>();
			scaller = GetComponent<CanvasScaler>();
			if (!animator)
			{
				return;
			}
			BBDevice cHDevice = BBDevice.iPhone;
			animator.enabled = false;
			if (cHDevice == BBDevice.iPad)
			{
				scaller.matchWidthOrHeight = 1f;
			}
			if (cHDevice == BBDevice.iPhoneX)
			{
				animator.SetTrigger(trigger + "Tall");
				return;
			}
			int num = Screen.height / Screen.width;
			if (num < 2)
			{
			}
		}
	}
}
