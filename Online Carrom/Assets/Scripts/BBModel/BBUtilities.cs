using System;
using System.Threading;

namespace BBModel
{
	public class BBUtilities
	{

		public static Random ThreadSafeRandom
		{
			get
			{
				Random result;
				if ((result = BBUtilities.RandomInstance) == null)
				{
					result = (BBUtilities.RandomInstance = new Random(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId));
				}
				return result;
			}
		}

		[ThreadStatic]
		private static Random RandomInstance;
	}
}
